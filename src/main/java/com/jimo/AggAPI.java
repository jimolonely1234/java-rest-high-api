package com.jimo;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * @author wangpeng
 * @func 一些聚合方法
 * @date 2018/8/16 8:01
 */
public class AggAPI {
    private RestHighLevelClient client;

    private String index = "blog";
    private String type = "java";

    @Before
    public void init() {
        client = API.getClientLocal();
    }

    /**
     * @func 平均数
     * @author wangpeng
     * @date 2018/8/16 8:05
     */
    @Test
    public void avgAgg() throws IOException {
        SearchRequest searchRequest = new SearchRequest(index);
        searchRequest.types(type);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(QueryBuilders.matchAllQuery());
        sourceBuilder.aggregation(
                AggregationBuilders.avg("avg_money").field("money")
        );
        searchRequest.source(sourceBuilder);
        SearchResponse response = client.search(searchRequest);
        Avg avgMoney = response.getAggregations().get("avg_money");
        System.out.println(avgMoney.getValue());//486.77659548995996
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/16 8:16
     */
    @Test
    public void multiAgg() throws IOException {
        SearchRequest request = new SearchRequest(index);
        request.types(type);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(QueryBuilders.matchAllQuery());
        sourceBuilder.aggregation(
                AggregationBuilders.terms("author_agg").field("author")
                        .subAggregation(
                                AggregationBuilders.sum("sum_views").field("pageViews")
                        )
        );
        request.source(sourceBuilder);
        SearchResponse response = client.search(request);
        Terms authorAgg = response.getAggregations().get("author_agg");
        for (Terms.Bucket bucket : authorAgg.getBuckets()) {
            System.out.println("-----------------------------");
            String key = bucket.getKeyAsString();
            long docCount = bucket.getDocCount();

            Sum sumViews = bucket.getAggregations().get("sum_views");
            System.out.println("key:" + key + ",docCount:" + docCount + ",sumViews:" + sumViews.getValue());
        }
    }



    @After
    public void close() throws IOException {
        client.close();
    }
}
