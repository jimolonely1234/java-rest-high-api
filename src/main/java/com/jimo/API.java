package com.jimo;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

/**
 * @author wangpeng
 * @func
 * @date 2018/8/15 17:04
 */
public class API {

    /**
     * @func 返回高级别客户端
     * @author wangpeng
     * @date 2018/8/15 16:42
     */
    public static RestHighLevelClient getRestHighLevelClient() {
        String[] hosts = "hadoop4:9200,hadoop5:9200,hadoop6:9200,hadoop7:9200,hadoop8:9200,hadoop9:9200".split(",");
        HttpHost[] httpHosts = new HttpHost[hosts.length];
        int i = 0;
        for (String host : hosts) {
            String ip = host.split(":")[0];
            String port = host.split(":")[1];
            httpHosts[i++] = new HttpHost(ip, Integer.parseInt(port), "http");
        }
        return new RestHighLevelClient(RestClient.builder(httpHosts));
    }

    /**
     * @func 使用本地ES测试，版本为最新版6.3.2
     * @author wangpeng
     * @date 2018/8/16 8:47
     */
    public static RestHighLevelClient getClientLocal() {
        return new RestHighLevelClient(RestClient.builder(new HttpHost("localhost", 9200)));
    }
}
