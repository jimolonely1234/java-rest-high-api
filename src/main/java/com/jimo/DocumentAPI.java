package com.jimo;

import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

/**
 * Created by ThinkPad on 2018/8/15.
 */
public class DocumentAPI {
    private RestHighLevelClient client;

    private String index = "blog";
    private String type = "java";

    @Before
    public void init() {
        client = API.getClientLocal();
    }

    /**
     * @func 生成随机数据
     * @author wangpeng
     * @date 2018/8/15 18:16
     */
    public BulkRequest generateData(int num) {
        BulkRequest bulkRequest = new BulkRequest();
        Random random = new Random();
        for (int i = 0; i < num; i++) {
            Map<String, Object> m = new HashMap<>();
            m.put("title", "java-blog-" + random.nextInt(1000000));
            m.put("content", random.nextInt(1000000000) + "java-content" + random.nextInt(100000000));
            m.put("money", random.nextDouble() * 1000);
            m.put("pageViews", random.nextInt(10000));
            m.put("author", "author" + random.nextInt(i + 2));
            m.put("support", "support" + random.nextInt(i + 10));
            m.put("isOld", random.nextBoolean());
            m.put("time", new Date());
            IndexRequest request = new IndexRequest(index, type, "" + i).source(m);
            bulkRequest.add(request);
        }
        return bulkRequest;
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/15 18:09
     */
    @Test
    public void insertData() throws IOException {
        XContentBuilder builder = XContentFactory.jsonBuilder();
        builder.startObject();
        {
            builder.field("user", "kimchy");
            builder.timeField("postDate", new Date());
            builder.field("message", "trying out Elasticsearch");
        }
        builder.endObject();

        IndexRequest indexRequest = new IndexRequest(index, type, "1").source(builder);
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/15 18:31
     */
    @Test
    public void bulkInsert() throws IOException {
        BulkResponse response = client.bulk(generateData(20));
        if (!response.hasFailures()) {
            System.out.println("ok");
        }
    }

    /**
     * @func
     * @author wangpeng
     * @date 2018/8/15 18:39
     */
    @Test
    public void getById() throws IOException {
        GetRequest request = new GetRequest(index, type, "1");
        GetResponse response = client.get(request);
        if (response.isExists()) {
            System.out.println(response.getSourceAsString());
        }
    }

    /**
     * @func 通过脚本更新
     * @author wangpeng
     * @date 2018/8/15 18:43
     */
    @Test
    public void updateDoc() throws IOException {
        UpdateRequest updateRequest = new UpdateRequest(index, type, "0");
        Map<String, Object> map = new HashMap<>();
        map.put("count", 100);
        Script inline = new Script(ScriptType.INLINE, "painless",
                "ctx._source.pageViews+=params.count", map);
        updateRequest.script(inline);
        UpdateResponse response = client.update(updateRequest);
        if (response.getResult() == DocWriteResponse.Result.UPDATED) {
            System.out.println("updated ok");
        }
    }


    @After
    public void close() throws IOException {
        client.close();
    }

}
