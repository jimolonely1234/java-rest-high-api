package com.jimo;

import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * @author wangpeng
 * @func
 * @date 2018/8/15 17:06
 */
public class IndexAPI {
    private RestHighLevelClient client;

    private String index = "blog";

    @Before
    public void init() {
        client = API.getClientLocal();
    }

    @Test
    public void indexExist() throws IOException {
        GetIndexRequest getIndexRequest = new GetIndexRequest();
        getIndexRequest.indices(index);
        boolean exists = client.indices().exists(getIndexRequest);
        System.out.println(exists);
    }


    /**
     * @func 创建index时指定mapping，setting的多种方式
     * @author wangpeng
     * @date 2018/8/15 17:14
     */
    @Test
    public void createIndex() throws IOException {
        CreateIndexRequest request = new CreateIndexRequest(index);
        request.settings(
                Settings.builder()
                        .put("index.number_of_shards", 3)
                        .put("index.number_of_replicas", 2)
        );

//        request.mapping("tweet",
//                "{\n" +
//                        "  \"tweet\": {\n" +
//                        "    \"properties\": {\n" +
//                        "      \"message\": {\n" +
//                        "        \"type\": \"text\"\n" +
//                        "      }\n" +
//                        "    }\n" +
//                        "  }\n" +
//                        "}",
//                XContentType.JSON);


//        XContentBuilder builder = XContentFactory.jsonBuilder();
//        builder.startObject();
//        {
//            builder.startObject("tweet");
//            {
//                builder.startObject("properties");
//                {
//                    builder.startObject("content");
//                    {
//                        builder.field("type", "text");
//                    }
//                    builder.endObject();
//                    builder.startObject("title");
//                    {
//                        builder.field("type", "text");
//                    }
//                    builder.endObject();
//                }
//                builder.endObject();
//            }
//            builder.endObject();
//        }
//        builder.endObject();

//        request.mapping(index, builder);

        request.mapping(
                "java",
                "title", "type=text",
                "content", "type=text",
                "pageViews", "type=long",
                "money", "type=double",
                "isOld", "type=boolean",
                "author", "type=keyword",
                "support", "type=keyword",
                "time", "type=date"
        );
//        request.mapping("java", "content", "type=text");
//        request.mapping("java", "pageViews", "type=long");
//        request.mapping("java", "money", "type=double");
//        request.mapping("java", "isOld", "type=boolean");
//        request.mapping("java", "author", "type=keyword");
//        request.mapping("java", "time", "type=date");

        CreateIndexResponse response = client.indices().create(request);
        if (response.isAcknowledged()) {
            System.out.println("ok");
        } else {
            System.out.println("failed");
        }
    }

    /**
     * @func 增加新的mapping
     * @author wangpeng
     * @date 2018/8/15 17:56
     */
    @Test
    public void putMapping() {

    }

    @After
    public void close() throws IOException {
        client.close();
    }
}
