package com.jimo;

import org.elasticsearch.action.search.*;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * @author wangpeng
 * @func
 * @date 2018/8/15 18:58
 */
public class SearchAPI {
    private RestHighLevelClient client;

    private String index = "blog";
    private String type = "java";

    @Before
    public void init() {
        client = API.getClientLocal();
    }


    /**
     * @func
     * @author wangpeng
     * @date 2018/8/15 18:59
     */
    @Test
    public void matchAll() throws IOException {
        SearchRequest searchRequest = new SearchRequest(index);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(QueryBuilders.matchAllQuery());
        searchRequest.source(sourceBuilder);
        searchRequest.types(type);
        SearchResponse response = client.search(searchRequest);
        printResponse(response);
    }


    /**
     * @func 分页查询
     * @author wangpeng
     * @date 2018/8/15 19:25
     */
    @Test
    public void scrollSearch() throws IOException {
        SearchRequest searchRequest = new SearchRequest(index);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(QueryBuilders.matchAllQuery());
        sourceBuilder.size(2);
        searchRequest.source(sourceBuilder);
        Scroll scroll = new Scroll(TimeValue.timeValueMinutes(1L));
        searchRequest.scroll(scroll);

        SearchResponse response = client.search(searchRequest);
        printResponse(response);
        String scrollId = response.getScrollId();
        SearchHit[] hits = response.getHits().getHits();
        while (hits != null && hits.length > 0) {
            SearchScrollRequest searchScrollRequest = new SearchScrollRequest(scrollId);
            searchScrollRequest.scroll(scroll);
            SearchResponse searchResponse = client.searchScroll(searchScrollRequest);
            scrollId = searchResponse.getScrollId();
            hits = searchResponse.getHits().getHits();
            printResponse(searchResponse);
        }

        //clear scroll
        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
        clearScrollRequest.addScrollId(scrollId);
        ClearScrollResponse clearScrollResponse = client.clearScroll(clearScrollRequest);
        System.out.println(clearScrollResponse.isSucceeded());
    }


    /**
     * @func
     * @author wangpeng
     * @date 2018/8/15 19:06
     */
    private void printResponse(SearchResponse response) {
        System.out.println("----------print-----------");
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }



    @After
    public void close() throws IOException {
        client.close();
    }
}
